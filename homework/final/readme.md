使用 Date 物件完成行事曆

# Date物件

當前時間:new Date()

指定時間:new Date(year,month,day,hours,muites,seconds,milliseconds)

* 方法

* 取得星期幾 : getDay()

* 取得日期 : getDate()

* 取得月份 : getMonth()

* 取得年分 : getFullYear()

* 設定日期 : setDate(date)

* 設定月份 setMonth(month)